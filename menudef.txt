OptionMenu HDBabuinito
{

	Title "Babuinito Options"
	StaticText "Spawn Rate (# of Babuins per 1 Babuinito)"
	ScaleSlider "Babuinito Spawn Rate","hd_babuinito_spawn",-1,20,1,"Replace All","Disabled"
	ScaleSlider "Cloaked Babuinito Spawn Rate","hd_specbabuinito_spawn",-1,20,1,"Replace All","Disabled"
	StaticText ""
	StaticText "Corpse Options"
	ScaleSlider "Dead Babuinito Spawn Rate","hd_deadbabuinito_spawn",-1,20,1,"Replace All","Disabled"
	ScaleSlider "Dead Cloaked Babuinito Spawn Rate","hd_deadspecbabuinito_spawn",-1,20,1,"Replace All","Disabled"
	StaticText ""
	Command "Replace All", "set hd_babuinito_spawn 0; set hd_specbabuinito_spawn 0; set hd_deadbabuinito_spawn 0; set hd_deadspecbabuinito_spawn 0;"
	Command "Disable All", "set hd_babuinito_spawn -1; set hd_specbabuinito_spawn -1; set hd_deadbabuinito_spawn -1; set hd_deadspecbabuinito_spawn -1;"
	SafeCommand "Reset Options", "resetcvar hd_babuinito_spawn; resetcvar hd_specbabuinito_spawn; resetcvar hd_deadbabuinito_spawn; resetcvar hd_deadspecbabuinito_spawn;"
/*	StaticText ""
	Option "Cloaked Babuinito Spawn Rate: ", "hd_specbabuinito_spawn", "SpawnRate"
	SafeCommand "Reset Option", "resetcvar hd_specbabuinito_spawn"
	StaticText ""
	Option "Dead Babuinito Spawn Rate:         ", "hd_deadbabuinito_spawn", "SpawnRate"
	SafeCommand "Reset Option", "resetcvar hd_deadbabuinito_spawn"
	StaticText ""
	Option "Dead Cloaked Babuinito Spawn Rate: ", "hd_deadspecbabuinito_spawn", "SpawnRate"
	SafeCommand "Reset Option", "resetcvar hd_deadspecbabuinito_spawn"
*/
}


AddOptionMenu "HDAddonMenu"
{
	Submenu "$TAG_BABUINITOMENU", "HDBabuinito"
}